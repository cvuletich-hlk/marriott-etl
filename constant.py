BASE_URL = "https://gatewaydsapprd.marriott.com"
OFFSET = 0
LIMIT = 10

HEADERS = {
    "Authorization": "Basic YnJhbmQtc2l0ZXM6UHM5MXN5VnZwSE5HWndrbEM3eEhwempRM3lqU2draktxaDQ3Uk4yOFZJZGVKbE5mN0ZqQ1ZycUh3bldVZmt4Zw==",
    "Accept-Encoding": "identity",
    "Accept-Language": "'en-US', 'es-ES', 'fr-FR', 'de-DE', 'it-IT','pt-BR', 'ja-JP', 'zh-CN', 'ru-RU'",
    "Content-Type": "application/json",
    "Cache-Control": "no-cache"
}